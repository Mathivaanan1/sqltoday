create database mathi;
drop database mathi;
use mathi;
create table student(
id int primary key,
name varchar(10),
gpa decimal(3,2)

);
describe student;
drop table student;
alter table student add column department varchar(5);
alter table student drop column department;
insert into student values(1,"mathi",5.6),(2,"sakthi",3.5),(3,"surya",2.5),(4,"tamil",1.8);
select * from student;
insert into student(id,name)values(5,"raja");
select * from student;
create table employee(
id int primary key,
ename varchar(10),
job_desc varchar(10),
salary int
);
describe employee;
insert into employee values(1,"mathi","CEO",100000);
insert into employee values(2,"mass","CEO",100000);
insert into employee values(3,"tamil","MANAGER",200000);
insert into employee values(4,"sakthi","MANAGER",300000);
insert into employee values(5,"tamil","SALES",500000);
insert into employee values(6,"surya","SALES",600000);
insert into employee values(7,"raja","HR",800000);
insert into employee values(8,"kabali","HR",1200000);
insert into employee values(9,"mani","ENGINEER",400000);
insert into employee values(10,"deena","ENGINEER",900000);

select * from employee;
select * from employee where salary>100000;
select * from employee where job_desc="CEO";
select * from employee where salary>400000 and job_desc="SALES";

select * from employee where job_desc in("MANAGER","HR");
select * from employee where job_desc not in("HR","MANAGER");

select * from employee where salary between 400000 and 900000;
select * from employee limit 5;
select * from employee where ename like 's%';
select * from employee where ename like '%s%';
select * from employee where ename like '___s';
update employee set job_desc="PILOT" where job_desc="HR";
select * from employee;
update employee set job_desc="PILOT" where job_desc="HR";
update employee set job_desc="HR" where id=6;
delete from employee where id=10;
select * from employee;
select distinct job_desc from employee;
select * from employee order by ename;
select * from employee where salary=100000 order by ename;

-- custom oder
select * from employee order by(case job_desc
when 'CEO' then 1
when 'MANAGER' then 2
when 'HR' then 3
else 100 end
),salary;
 select count(*) from employee;
select count(*) from employee where job_desc="HR";

select * from employee;
use mathi;
select ucase(ename) NAME,salary from employee;
select upper(ename) NAME,salary from employee;
select lower(ename) NAME,salary from employee;

select upper(ename) NAME,salary from employee;
select ename,char_length(ename) CHAR_COUNT from employee;
select ename,character_length(ename) CHAR_COUNT from employee;

select ename,concat('RS.',salary) from employee;
select ename,concat('RS.',format( salary,0)) from employee;

select ename,job_desc from employee;
select ename, left(job_desc,2) from employee;

select lcase(ename) NAME from employee;
alter table employee add column join_date date;
select * from employee;
update employee set join_date="2023-04-03" where id in(1,2,3,4,5,6,7,8,9,10);
update employee set join_date="2023-04-01" where id in(1,2,5,6,10);
select * from employee;
select now();
select date(now());
select current_timestamp();
select current_date();
select date_format(curdate(),"%d/%m/%y");
select datediff(curdate(),"2023/03/02")as days;
select date_add(curdate(),INTERVAL 1 week);
select * from employee order by job_desc;
select job_desc,avg(salary) from employee group by job_desc;
select job_desc,count(id) from employee group by job_desc; 
select job_desc,count(id) from employee group by job_desc having count(id)>2; 
select job_desc,count(id) from employee group by job_desc having count(id)>1 order by count(id); 
select job_desc,count(id) from employee where salary>500000 group by job_desc having count(id)>1 order by count(id); 

create table employee1(
id int primary key auto_increment,
ename varchar(10) not null,
job_desc varchar(10) default 'unasigned',
salary int,
pan varchar(10) unique

);
insert into employee1 (ename,salary) values("mathi",3000);
select * from employee1;
drop table employee1;

create table branch(
branch_id int primary key auto_increment,
br_name  varchar(20) not null,
addr varchar(100)
);

create table employee1(
id int primary key auto_increment,
ename varchar(10) not null,
job_desc varchar(10),
salary int,
branch_id int,
constraint fk_branchid foreign key(branch_id) references branch(branch_id)

);
show index from employee1;
create index name_index on employee1(ename);
alter table employee drop index name_index;
show index from employee1;
alter table employee1 drop index name_index;

insert into branch values(1,"madurai","110 banajaa street");
insert into branch values(2,"chennai","123 banajaa street");
insert into branch values(3,"kovai","144 banajaa street");

select * from employee1;
insert into employee1 values(1,"mathi","CEO",100000,1);
insert into employee1 values(2,"mass","CEO",100000,2);
insert into employee1 values(3,"tamil","MANAGER",200000,3);
insert into employee1 values(4,"sakthi","MANAGER",300000,3);
insert into employee1 values(5,"tamil","SALES",500000,1);
insert into employee1 values(6,"surya","SALES",600000,2);
insert into employee1 values(7,"raja","HR",800000,3);
insert into employee1 values(8,"kabali","HR",1200000,2);
insert into employee1 values(9,"mani","ENGINEER",400000,1);
insert into employee1 values(10,"deena","ENGINEER",900000,1);

select * from employee1;
select * from branch;

select employee1.id,employee1.ename,employee1.job_desc,branch.br_name from employee1 inner join branch on employee1.branch_id=branch.branch_id order by employee1.id;
select employee1.id,employee1.ename,employee1.job_desc,branch.br_name from employee1 right join branch on employee1.branch_id=branch.branch_id order by employee1.id;

create table cites(
branch_id int primary key auto_increment,
br_name  varchar(20) not null,
addr varchar(100)
);

insert into cites values(1,"madurai","110 banajaa street");
insert into cites values(2,"chennai","123 banajaa street");
insert into cites values(3,"pazhani","144 banajaa street");
insert into cites values(4,"bigar","144 banajaa street");

select * from cites union select * from branch;
select * from cites union all select * from branch;

select * from employee1 where branch_id=(select branch_id from branch where br_name="chennai");

select * from employee;

select * from employee1 where salary=(select min(salary) from employee1);

select branch_id,br_name from branch where exists (select * from employee1 where job_desc="HR");
select branch_id,br_name from branch where exists (select * from employee1 where job_desc="sup");
select branch_id,br_name from branch where (select * from employee1 where job_desc="sup");
 
 select * from branch where branch_id=any(select branch_id from employee1 where salary>700000);
 
 select * from  employee1 where branch_id <>all (select branch_id from branch where br_name in("chennai","madurai")); 
 
select employee1.id,employee1.ename,employee1.job_desc,branch.br_name from employee1 inner join branch on employee1.branch_id=branch.branch_id order by employee1.id;
create view emp_br as select employee1.id,employee1.ename,employee1.job_desc,branch.br_name from employee1 inner join branch on employee1.branch_id=branch.branch_id order by employee1.id;
select * from emp_br;
select * from emp_br where job_desc="MANAGER";

















